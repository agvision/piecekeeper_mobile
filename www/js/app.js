// Ionic Starter App


// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic','ionic.service.core','ionic.service.analytics', 'ionic.ui.superSlideBox', 'ngCordova'], function($httpProvider) {
  // Use x-www-form-urlencoded Content-Type
  $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

  /**
   * The workhorse; converts an object to x-www-form-urlencoded serialization.
   * @param {Object} obj
   * @return {String}
   */
  var param = function(obj) {
    var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

    for(name in obj) {
      value = obj[name];

      if(value instanceof Array) {
        for(i=0; i<value.length; ++i) {
          subValue = value[i];
          fullSubName = name + '[' + i + ']';
          innerObj = {};
          innerObj[fullSubName] = subValue;
          query += param(innerObj) + '&';
        }
      }
      else if(value instanceof Object) {
        for(subName in value) {
          subValue = value[subName];
          fullSubName = name + '[' + subName + ']';
          innerObj = {};
          innerObj[fullSubName] = subValue;
          query += param(innerObj) + '&';
        }
      }
      else if(value !== undefined && value !== null)
        query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
    }

    return query.length ? query.substr(0, query.length - 1) : query;
  };

  // Override $http service's default transformRequest
  $httpProvider.defaults.transformRequest = [function(data) {
    return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
  }];
})


.run(function($ionicPlatform, $ionicAnalytics, $cordovaStatusbar, $rootScope, $state, $location, $ionicSideMenuDelegate, httpService, localService, GAService, StringsService, $window) {

  $ionicAnalytics.register();
  
  // get strings for app
  $rootScope.strings = StringsService.getStrings();

  // deactivate back button for login page
  $rootScope.$on('$locationChangeStart', function(event, next, current){ 

      var allowed = ["/forgot", "/register", "/login", "/social"];

      var data = current.split("#");
      var current_path = data[1];

      var data = next.split("#");
      var next_path = data[1];

      if (current_path == "/login" && allowed.indexOf(next_path) < 0 && (typeof $rootScope.logged_in == "undefined" || !$rootScope.logged_in)) {
        event.preventDefault();            
      }
  });

  // track menu open event
  $rootScope.$watch(function(){
    return $ionicSideMenuDelegate.getOpenRatio();
  }, function(value){
    if (value == 1) {
      GAService.trackEvent("Menu", "Opened", null, null);
    }
  });

  $rootScope.$on('$locationChangeSuccess', function() {

      GAService.trackPage($location.path());

      $rootScope.oldLocation = $rootScope.actualLocation;
      $rootScope.actualLocation = $location.path();
  });
  // redirect to desktop version
  $rootScope.$on("profile", function(){
    localService.get("profile", function(profile){
      if (parseInt(profile.level.value) > 1) {
        $window.location.href = "https://piecekeeper.onepiece.com/admin";
      }
    });
  });

  $ionicPlatform.ready(function() {

    
    // init Facebook sdk
    FB.init({ 
      appId: '161689280703003',
      status: true, 
      cookie: true, 
      xfbml: true,
      version: 'v2.3'
    });

    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    // Open the login modal
    $rootScope.showLogin = function() {
      $location.path("/login");
    };

    $rootScope.showDashboard = function() {
      $location.path("/dashboard");
    };

    $rootScope.doLogout = function() {
      httpService.logout(function(){
        $rootScope.showLogin();
      });
    };
    
    httpService.checkLogin(function(){
      if (!$location.path().match(/social/) && !$location.path().match(/profile/)) {
        $rootScope.showDashboard();
      }
    }, function(){
      if ($location.path().match(/app/)) {
        $rootScope.showLogin();
      }
    });

  });
})

.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}])

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl'
  })

  .state('login', {
    url: "/login",
    templateUrl: "templates/login.html",
    controller: 'LoginCtrl'
  })

  .state('invitation', {
    url: "/register/:invitation",
    templateUrl: "templates/register.html",
    controller: 'RegisterCtrl'
  })

  .state('register', {
    url: "/register",
    templateUrl: "templates/register.html",
    controller: 'RegisterCtrl'
  })

  .state('forgot', {
    url: "/forgot",
    templateUrl: "templates/forgot.html",
    controller: 'ForgotCtrl'
  })

  .state('reset', {
    url: "/reset/:hash",
    templateUrl: "templates/reset.html",
    controller: 'ResetCtrl'
  })

  .state('terms', {
    url: "/terms",
    templateUrl: "templates/terms.html",
    controller: 'TermsCtrl'
  })

  .state('social', {
    url: "/social",
    templateUrl: "templates/social.html",
    controller: 'SocialCtrl'
  })

  .state('oAuthCallback1', {
    url: "/social/:network/:param1",
    templateUrl: "templates/social.html",
    controller: 'SocialCtrl'
  })

  .state('oAuthCallback2', {
    url: "/social/:network/:param1/:param2",
    templateUrl: "templates/social.html",
    controller: 'SocialCtrl'
  })

  .state('app.dashboard', {
    url: "/dashboard",
    views: {
      'menuContent': {
        templateUrl: "templates/dashboard.html",
        controller: 'DashboardCtrl'
      }
    }
  })

  .state('app.levels', {
    url: "/levels",
    views: {
      'menuContent': {
        templateUrl: "templates/levels.html",
        controller: 'LevelsCtrl'
      }
    }
  })

  .state('app.ranking', {
    url: "/ranking",
    views: {
      'menuContent': {
        templateUrl: "templates/ranking.html",
        controller: 'RankingCtrl'
      }
    }
  })

  .state('app.goals', {
    url: "/goals",
    views: {
      'menuContent': {
        templateUrl: "templates/goals.html",
        controller: 'GoalsCtrl'
      }
    }
  })

  .state('app.missions', {
    url: "/missions",
    views: {
      'menuContent': {
        templateUrl: "templates/missions.html",
        controller: 'MissionsCtrl'
      }
    }
  })

  .state('app.codes', {
    url: "/codes",
    views: {
      'menuContent': {
        templateUrl: "templates/codes.html",
        controller: 'CodesCtrl'
      }
    }
  })

  .state('app.payouts', {
    url: "/payouts",
    views: {
      'menuContent': {
        templateUrl: "templates/payouts.html",
        controller: 'PayoutsCtrl'
      }
    }
  })

  .state('app.profile', {
    url: "/profile",
    views: {
      'menuContent': {
        templateUrl: "templates/profile.html",
        controller: 'ProfileCtrl'
      }
    }
  })

  .state('app.oAuthCallback1', {
    url: "/profile/:network/:param1",
    views: {
      'menuContent': {
        templateUrl: "templates/profile.html",
        controller: 'ProfileCtrl'
      }
    }
  })

  .state('app.oAuthCallback2', {
    url: "/profile/:network/:param1/:param2",
    views: {
      'menuContent': {
        templateUrl: "templates/profile.html",
        controller: 'ProfileCtrl'
      }
    }
  })

  .state('app.salesteam', {
    url: "/salesteam",
    views: {
      'menuContent': {
        templateUrl: "templates/salesteam.html",
        controller: 'SalesteamCtrl'
      }
    }
  })

  .state('app.blogs', {
    url: "/blogs/:blog",
    views: {
      'menuContent': {
        templateUrl: "templates/blog.html",
        controller: 'BlogsCtrl'
      }
    }
  });

  
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/dashboard');
})
