
angular.module('starter')

.controller('ForgotCtrl', function($scope, $stateParams, $rootScope, $location, httpService, $ionicPopup, $ionicNavBarDelegate, $ionicSideMenuDelegate) {

	$scope.forgotData = {};

	$scope.doRecover = function() {
	  	httpService.forgotPassword($scope.forgotData.email, function(data){
	  		// if forgot email has been sent
	    	if (data.sent) {
	    		// redirect to login and show alert
	    		$location.path("/login");
	      		$ionicPopup.alert({
	        		title: $rootScope.strings.forgot.alertTitle,
	        		template: $rootScope.strings.forgot.alertSentSuccess
	      		});
	    	} else {
	    		// show error alert
	      		$ionicPopup.alert({
	        		title: $rootScope.strings.forgot.alertTitle,
	        		template: $rootScope.strings.forgot.alertSentErrorEmail
	      		});
	    	}
	  	});
	};
});
