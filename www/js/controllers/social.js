
angular.module('starter')

.controller('SocialCtrl', function($scope, $rootScope, $stateParams, $window, localService, httpService, GAService, $ionicPopup, $location) {

	// Init Server Data
	httpService.init("getSocialNetworks", "networks");

	$scope.FBProfile = {};
	$scope.FBPages   = [];
	$scope.Pinterest = {};
	$scope.Vine      = {};


	$scope.chooseAccountPopup = null;

	// get Twitter user from oAuth Callback Request
	if (angular.isDefined($stateParams.network) && $stateParams.network == "twitter" && angular.isDefined($stateParams.param1) && angular.isDefined($stateParams.param2)) {
		httpService.authNetworkRequest({
			service: $stateParams.network,
			step: "getUser",
			oauth_token: $stateParams.param1,
			oauth_verifier: $stateParams.param2
		}, function(data){
			
			$scope.linkSocialAccount({
				network: "twitter",
				profile: "http://twitter.com/"+data.user.screen_name,
				followers: data.user.followers_count
			});
		
		}, function(){
			$ionicPopup.alert({
	  			title: $rootScope.strings.social.alertTitleLink,
	  			template: $rootScope.strings.social.alertLinkError
			});
		});
	}

	// get Instagram user from oAuth Callback Request
	if (angular.isDefined($stateParams.network) && $stateParams.network == "instagram" && angular.isDefined($stateParams.param1)) {

		httpService.authNetworkRequest({
			service: $stateParams.network,
			step: "getUser",
			code: $stateParams.param1
		}, function(data){
			
			$scope.linkSocialAccount({
				network: "instagram",
				profile: data.user.profile,
				followers: data.user.followers
			});
		
		}, function(){
			$ionicPopup.alert({
	  			title: $rootScope.strings.social.alertTitleLink,
	  			template: $rootScope.strings.social.alertLinkError
			});
		});
	}

	// get Tumblr user from oAuth Callback Request
	if (angular.isDefined($stateParams.network) && $stateParams.network == "tumblr" && angular.isDefined($stateParams.param1) && angular.isDefined($stateParams.param2)) {
		httpService.authNetworkRequest({
			service: $stateParams.network,
			step: "getUser",
			oauth_token: $stateParams.param1,
			oauth_verifier: $stateParams.param2
		}, function(data){
			
			$scope.linkSocialAccount({
				network: "tumblr",
				profile: data.user.profile,
				followers: data.user.followers
			});
		
		}, function(){
			$ionicPopup.alert({
	  			title: $rootScope.strings.social.alertTitleLink,
	  			template: $rootScope.strings.social.alertLinkError
			});
		});
	}

	// get Youtube user from oAuth Callback Request
	if (angular.isDefined($stateParams.network) && $stateParams.network == "youtube" && angular.isDefined($stateParams.param1) && angular.isDefined($stateParams.param2)) {
		httpService.authNetworkRequest({
			service: $stateParams.network,
			step: "getUser",
			state: $stateParams.param1.replace("----", "/"),
			code: $stateParams.param2.replace("----", "/")
		}, function(data){
			
			$scope.linkSocialAccount({
				network: "youtube",
				profile: data.user.profile,
				followers: data.user.followers
			});
		
		}, function(){
			$ionicPopup.alert({
	  			title: $rootScope.strings.social.alertTitleLink,
	  			template: $rootScope.strings.social.alertLinkError
			});
		});
	}

	// fb link click listener
	$scope.doFBLink = function(){
		GAService.trackEvent("Social", "Facebook", "clicked", null);

		FB.getLoginStatus(function(response){
		   	if (response.status != "connected") {
		    	FB.login(function(response){
		      		if (response.status == "connected") {
		        		$scope.getFBAccounts();
		      		} else {
		        		$ionicPopup.alert({
		          			title: $rootScope.strings.social.alertTitleFB,
		          			template: $rootScope.strings.social.alertLinkErrorFB
		        		});
		      		}
		    	}, {scope: 'public_profile, email, manage_pages'});
		  	} else {
		  		$scope.getFBAccounts();
		  	}
		});
	}

	$scope.doTWLink = function(){
		GAService.trackEvent("Social", "Twitter", "clicked", null);

		httpService.authNetworkRequest({
			service: "twitter",
			step: "redirectUrl"
		}, function(data){
			$window.location.href = data.url;
		}, function(){
			$ionicPopup.alert({
	  			title: $rootScope.strings.social.alertTitleTW,
	  			template: $rootScope.strings.social.alertLinkErrorTW
			});
		});
	}

	$scope.doINSTLink = function(){
		GAService.trackEvent("Social", "Instagram", "clicked", null);

		httpService.authNetworkRequest({
			service: "instagram",
			step: "redirectUrl"
		}, function(data){
			$window.location.href = data.url;
		}, function(){
			$ionicPopup.alert({
	  			title: $rootScope.strings.social.alertTitleINST,
	  			template: $rootScope.strings.social.alertLinkError
			});
		});
	}

	$scope.doTUBLink = function(){
		GAService.trackEvent("Social", "Tumblr", "clicked", null);

		httpService.authNetworkRequest({
			service: "tumblr",
			step: "redirectUrl"
		}, function(data){
			$window.location.href = data.url;
		}, function(){
			$ionicPopup.alert({
	  			title: $rootScope.strings.social.alertTitleTUB,
	  			template: $rootScope.strings.social.alertLinkError
			});
		});
	}

	$scope.doPINLink = function(){
		GAService.trackEvent("Social", "Pinterest", "clicked", null);

		$ionicPopup.show({
		    template: '<input type="text" ng-model="Pinterest.username">',
		    title: $rootScope.strings.social.popupTitlePIN,
		    scope: $scope,	
		    buttons: [
		      	{ text: 'Cancel' },
		      	{	
		        	text: '<b>Send</b>',
		        	type: 'button-positive',
		        	onTap: function(e) {
		          		if (angular.isUndefined($scope.Pinterest.username) || $scope.Pinterest.username.length == 0) {
		            		//don't allow the user to close unless he enters Pinterest username
		            		e.preventDefault();
		          		} else {
		            		httpService.authNetworkRequest({
		            			service: "pinterest",
		            			step: "getUser",
		            			username: $scope.Pinterest.username
		            		}, function(data){
		            			$scope.linkSocialAccount({
									network: "pinterest",
									profile: data.user.profile,
									followers: data.user.followers
								});
		            		}, function(){
		            			$ionicPopup.alert({
		            				title: $rootScope.strings.social.alertTitlePIN,
		            				template: $rootScope.strings.social.alertLinkErrorPIN
		            			});
		            		});
		          		}
		        	}
		      	}
		    ]
		})
	}

	$scope.doYBLink = function(){
		GAService.trackEvent("Social", "Youtube", "clicked", null);

		httpService.authNetworkRequest({
			service: "youtube",
			step: "redirectUrl"
		}, function(data){
			$window.location.href = data.url;
		}, function(){
			$ionicPopup.alert({
	  			title: $rootScope.strings.social.alertTitleINST,
	  			template: $rootScope.strings.social.alertLinkError
			});
		});
	}

	$scope.doVNLink = function(){
		GAService.trackEvent("Social", "Vine", "clicked", null);

		$ionicPopup.show({
		    template: '<input type="text" ng-model="Vine.userID">',
		    title: $rootScope.strings.social.popupTitleVN,
		    scope: $scope,	
		    buttons: [
		      	{ text: 'Cancel' },
		      	{	
		        	text: '<b>Send</b>',
		        	type: 'button-positive',
		        	onTap: function(e) {
		          		if (angular.isUndefined($scope.Vine.userID) || $scope.Vine.userID.length == 0) {
		            		//don't allow the user to close unless he enters Vine user ID
		            		e.preventDefault();
		          		} else {
		            		httpService.authNetworkRequest({
		            			service: "vine",
		            			step: "getUser",
		            			userID: $scope.Vine.userID
		            		}, function(data){
		            			$scope.linkSocialAccount({
									network: "vine",
									profile: data.user.profile,
									followers: data.user.followers
								});
		            		}, function(){
		            			$ionicPopup.alert({
		            				title: $rootScope.strings.social.alertTitleVN,
		            				template: $rootScope.strings.social.alertLinkErrorVN
		            			});
		            		});
		          		}
		        	}
		      	}
		    ]
		})
	}

	$scope.getFBAccounts = function(){
		var pages = [];
		FB.api("/me", function(response){
			$scope.FBProfile = response;

			FB.api("/me/accounts", function(response){
				if (angular.isDefined(response.data)) {
					$scope.FBPages = response.data
				}

				$scope.chooseFBAccount();
			});
		});
	}

	$scope.chooseFBAccount = function(){
		$scope.chooseAccountPopup = $ionicPopup.show({
			title: $rootScope.strings.social.alertTitleChooseFBAccount,
			scope: $scope,
			buttons: [
				{text: "Cancel"}
			],
			template: 	"<ion-list> \
					  		<ion-item ng-click='linkFBAccount()'> \
					    		{{FBProfile.first_name}} {{FBProfile.last_name}} \
					  		</ion-item> \
					  		<ion-item ng-repeat='page in FBPages' ng-click='linkFBPage(page.id)'> \
					    		{{page.name}} \
					  		</ion-item> \
						</ion-list>"
		});
	}

	$scope.linkSocialAccount = function(accountData){
		httpService.linkSocialNetwork(accountData, function(data){

			if (data.linked) {
				GAService.trackEvent("Social", accountData.network, "linked", null);

				httpService.getSocialNetworks();

				if (accountData.network == "facebook") {
					$scope.chooseAccountPopup.close();
				}
			}

		}, function(data){
			var message = $rootScope.strings.social.alertLinkError;
			if (data.error_code == 2) {
				message = $rootScope.strings.social.alertLinkErrorLinked;
			}

			$ionicPopup.alert({
				title: $rootScope.strings.social.alertTitleLink,
				template: message
			});
		});
	}

	$scope.linkFBAccount = function(){
		FB.api("/me/friends", function(response){

			if (angular.isDefined(response.summary) && angular.isDefined(response.summary.total_count)) {
				$scope.FBProfile.friends = response.summary.total_count;

				$scope.linkSocialAccount({
					network: "facebook",
					profile: $scope.FBProfile.link,
					followers: $scope.FBProfile.friends
				});
			}			
		});
	}

	$scope.linkFBPage = function(pageID){
		FB.api("/"+pageID, function(response){
			$scope.linkSocialAccount({
				network: "facebook",
				profile: response.link,
				followers: response.likes
			});
		});
	}

	$scope.completeRegistration = function(){
		httpService.completeRegistration(function(data){
			$location.path("/login");

			GAService.trackEvent("Register", "PieceKeepers", "completed", null);

    		$ionicPopup.alert({
	      		title: $rootScope.strings.social.alertTitleRegisterSuccess,
	      		template: $rootScope.strings.social.alertRegisterSuccess
    		});
		}, function(data){
			var message = ""

			if (data.error_code == 1) {
				message = $rootScope.strings.social.alertRegisterErrorUser
			} 

			else if (data.error_code == 2) {
				message = $rootScope.strings.social.alertRegisterErrorNetwork
			}

    		$ionicPopup.alert({
	      		title: $rootScope.strings.social.alertTitleRegister,
	      		template: message
    		});
		});
	}

	$scope.showNetworkFollowers = function(network){
		if (angular.isDefined($rootScope.networks) && network in $rootScope.networks) {
			return true;
		}

		return false;
	}
});
