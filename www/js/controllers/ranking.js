
angular.module('starter')

.controller('RankingCtrl', function($scope, $rootScope, $stateParams, $window, localService, httpService) {

	// Init Server Data
	httpService.init("getGlobalRanking", "globalRanking", "logged_in");
	httpService.init("getProfile", "profile", "logged_in");

	var scroll_view;
	var window_height = $window.innerHeight;

	// compute global top list height
	ionic.Platform.ready(function(){
		scroll_view = angular.element(document.getElementsByClassName("scroll-view"));
		
		var height  = window_height - 262; 

		scroll_view.css({
			"height" : height+"px"
		});
	});

	// init layout details
	localService.get("profile", function(profile){
		var types = ["global", "country", "city"];

		for (var i in types) {
			if (profile.ranking[types[i]].last_month > 0) {
				$rootScope.profile.ranking[types[i]].arrow = "success ion-arrow-up-c";
				$rootScope.profile.ranking[types[i]].text = $rootScope.strings.ranking.labelLastMonth;
			} else if (profile.ranking[types[i]].last_month == 0) {
				$rootScope.profile.ranking[types[i]].arrow = "success ion-arrow-up-c";
				$rootScope.profile.ranking[types[i]].text = $rootScope.strings.ranking.labelSameLastMonth;
			} else {
				$rootScope.profile.ranking[types[i]].arrow = "error ion-arrow-down-c";
				$rootScope.profile.ranking[types[i]].text = $rootScope.strings.ranking.labelLastMonth;
			}
		}
	});

});
