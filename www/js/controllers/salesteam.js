
angular.module('starter')

.controller('SalesteamCtrl', function($scope, $rootScope, $stateParams, $window, localService, httpService, $ionicPopup) {

	// Init Server Data
	httpService.init("getSalesTeam", "salesteam", "logged_in");

	$scope.invitationData = {};

	localService.get("salesteam", function(salesteam){

		for (var i = salesteam.invitations.length - 1; i >= 0; i--) {
			if (salesteam.invitations[i].approved == 1) {
				salesteam.invitations[i].status = $rootScope.strings.salesteam.labelStatusAccepted;
			} else {
				salesteam.invitations[i].status = $rootScope.strings.salesteam.labelStatusPending;
			}
		}
	});

	$scope.sendInvitation = function(){
		if (angular.isUndefined($scope.invitationData.email) || $scope.invitationData.email.length == 0) {
			$ionicPopup.alert({
			  title: $rootScope.strings.salesteam.alertTitleInvite,
			  template: $rootScope.strings.salesteam.alertInviteErrorEmail
			});  
		} else {
			$ionicPopup.confirm({
			  title: $rootScope.strings.salesteam.confirmTitleInvite,
			  template: $rootScope.strings.salesteam.confirmTitleInvite
			}).then(function(confirm){
				if (confirm) {

					// send invitation request to server
					httpService.sendTeamInvitation($scope.invitationData, function(data){
						if (data.invited) {
							$scope.invitationData = {};
							httpService.getSalesTeam(function(){
								$ionicPopup.alert({
								  title: $rootScope.strings.salesteam.alertTitleInvite,
								  template: $rootScope.strings.salesteam.alertInviteSuccess
								});  
							});

							
						}
					}, function(data){
						if ("errors" in data) {
							$scope.showAlert($rootScope.strings.salesteam.alertTitleInvite, data.errors);
						}
					});
				}
			});
		}
	}
});
