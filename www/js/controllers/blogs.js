
angular.module('starter')

.controller('BlogsCtrl', function($scope, $rootScope, $stateParams, $window, localService) {

	localService.get("blogs", function(blogs){
		var id_blog = $stateParams.blog;
		$scope.blog = $rootScope.blogs[id_blog];

		// remove anchors from images
		for (var i = 0; i < blogs.length; i++) {
			$rootScope.blogs[i].body = $rootScope.removeImageAnchors(blogs[i].body);
		}
	});
	
});
