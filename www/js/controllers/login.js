
angular.module('starter')

.controller('LoginCtrl', function($scope, $stateParams, $rootScope, httpService, GAService, $ionicPopup, $location, $ionicNavBarDelegate, $ionicSideMenuDelegate, $timeout) {

	$scope.loginData = {};

	/*
	* Tries user authentication based on email and password
	*/
	$scope.doLogin = function() {

		// check credentials on server
	  	httpService.login($scope.loginData, function(data){
	  		GAService.trackLoginEvent("success", null);

	  		// on success redirect to dashboard
	    	$location.path("/app/dashboard");

	  	}, function(status){
	  		// on error display proper message
	    	if (status == "invalid") {

	    		GAService.trackLoginEvent("error", "invalid_login");

	      		$ionicPopup.alert({
	        		title: $rootScope.strings.login.alertTitle,
	        		template: $rootScope.strings.login.alertErrorInvalidEmail
	      		});
	    	} else {

	    		GAService.trackLoginEvent("error", "blocked_ip");

	      		$ionicPopup.alert({
	        		title: $rootScope.strings.login.alertTitle,
	        		template: $rootScope.strings.login.alertErrorBlocked
	      		});
	    	}
	  	});
	};

	// Tries Facebook login
	$scope.doFBLogin = function(){
		FB.getLoginStatus(function(response){
		   	if (response.status != "connected") {
		    	FB.login(function(response){
		      		if (response.status == "connected") {
		      			// send Facebook access token to server
		        		$scope.handleFBLogin(response.authResponse.accessToken);
		      		} else {
		        		$ionicPopup.alert({
		          			title: $rootScope.strings.login.alertTitleFB,
		          			template: $rootScope.strings.login.alertErrorFB
		        		});
		      		}
		    	}, {scope: 'public_profile, email, user_birthday, user_education_history, user_location'});
		  	} else {
		  		// send Facebook access token to server
		  		$scope.handleFBLogin(response.authResponse.accessToken);
		  	}
		});
	};

	// Send facebook access token to server
	$scope.handleFBLogin = function(accessToken){
		httpService.FBLogin({
			access_token: accessToken
		}, function(data){
			$location.path("/app/dashboard");
		}, function(data) {
			$scope.showAlert($rootScope.strings.login.alertTitleFB, data.errors);
			$location.path("/register");
		});
	}

	$scope.showAlert = function(title, errors){
		var message = "";

		for (var i in errors) {
			message += "<p>" + errors[i] + "</p>";
		}

		var alertPopup = $ionicPopup.alert({
		    title: title,
		    template: message
		});
	}
});
