
angular.module('starter')

.controller('LevelsCtrl', function($scope, $rootScope, $stateParams, localService, httpService, $timeout) {

	// Init Server Data
	httpService.init("getProfile", "profile", "logged_in");
	httpService.init("getLevels", "levels", "profile");

	// init levels layout details
	localService.get("profile", function(profile){
		localService.get("levels", function(levels){
			var level = parseInt(profile.level.value) + 3;
			if (level > 3) {
				level = 3;	
			}  
			
			// display check mark for current user level
			for (var i = 1; i <= 3; i++) {
				if (level == i) {
					$rootScope.levels["level_"+i].class="ion-ios-checkmark";
				} else {
					$rootScope.levels["level_"+i].class="sign-cover";
				}
			}
		});		
	});

});
