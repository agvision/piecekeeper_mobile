
angular.module('starter')

.controller('CodesCtrl', function($rootScope, $scope, $stateParams, $window, localService, httpService, $ionicPopup, $timeout, GAService) {

	// Init Server Data
	httpService.init("getDiscountCodes", "codes", "logged_in");

	var scroll_view, height;
	var window_height = $window.innerHeight;

	$scope.codeData = {};

	localService.get("profile", function(profile){

		// init layout and labels
		if (profile.level.value > -2) {
			$scope.code_type = $rootScope.strings.codes.labelMultiuseType;
			$scope.codeData.type = 2;
			height  = window_height - 389;
		} else {
			$scope.code_type = $rootScope.strings.codes.labelSingleuseType;
			$scope.codeData.type = 1;
			height  = window_height - 327;
		}

		scroll_view = angular.element(document.getElementsByClassName("table-body")); 
		scroll_view.css({
			"min-height" : height+"px"
		});
	});

	// creates a Discount Code in the PieceKeeper System
	$scope.createCode = function(){
		GAService.trackEvent("Code", "Creation", "clicked", null);

		httpService.postCode($scope.codeData, function(data){
			if (data.created) {
				GAService.trackEvent("Code", "Creation", "success", null);
				$scope.showAlert([$rootScope.strings.codes.labelCreatedSuccess]);

				// refresh discount codes Data
				httpService.getDiscountCodes();

				$scope.codeData = {};
			} else {
				$scope.showAlert($rootScope.strings.codes.alertTitle, data.errors);
			}
		});
	};
});
