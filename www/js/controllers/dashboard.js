
angular.module('starter')

.controller('DashboardCtrl', function($scope, $rootScope, $stateParams, $ionicSlideBoxDelegate, $timeout, $window, httpService, localService, $location, GAService) {
	
	// Init Server data
	httpService.init("getBlogs", "blogs", "logged_in", 5);
	httpService.init("getProfile", "profile", "logged_in");
	
	var sliderSwipe = false;
	var window_height = $window.innerHeight;
	var slider, slider_slides, slider_slide;

	// save user level here
	$scope.level = "";

	// determine current user level
	localService.get("profile", function(profile){
		var level = parseInt(profile.level.value) + 3;
		$scope.level = (level > 3) ? 3 : level;
	});

	// cache useful dom elements
	var getDOMElements = function(){
		slider        = angular.element(document.getElementsByClassName("slider"));
		slider_slides = angular.element(document.getElementsByClassName("slider-slides"));
		slider_slide  = angular.element(document.getElementsByClassName("slider-slide"));
	}

	// enable/disable dashboard scrolling
	var enableDashboardScroll = function(){
		slider.css({
			"top" : (window_height - 44)+"px"
		});
		slider_slides.css({
			"margin-top" : "0px"
		});
	};

	var disableDashboardScroll = function(){
		slider.css({
			"top" : 44+"px"
		});
		slider_slides.css({
			"margin-top" : (window_height - 2*44)+"px"
		});
	}

	// enable/disable news slider
	var enableNewsSlide = function(){
		$ionicSlideBoxDelegate.$getByHandle("news-slider").enableSlide(true);
	}

	var disableNewsSlide = function(){
		$ionicSlideBoxDelegate.$getByHandle("news-slider").enableSlide(false);
	}

	// set slider height
	var setSliderHeight = function(height){
		$scope.sliderHeight = {"height" : height+"px"};
	}

	setSliderHeight(window_height - 44);

	getDOMElements();
	enableDashboardScroll();

	ionic.Platform.ready(function(){
		getDOMElements();
		enableDashboardScroll();
	});

	// events 
	$scope.onNewsHeaderTouch = function(){
		GAService.trackEvent("News", null, "clicked", null);
		disableDashboardScroll();
		enableNewsSlide();
	}

	$scope.onNewsHeaderRelease = function(){
		var index = $ionicSlideBoxDelegate.$getByHandle("news-slider").currentIndex();

		if (!sliderSwipe) {
			if (index == 0) {
				// $ionicSlideBoxDelegate.$getByHandle("news-slider").slide(1);
			}
			else if (index == 1) {
				// $ionicSlideBoxDelegate.$getByHandle("news-slider").slide(0);
			}
		}

		sliderSwipe = false;
	}

	$scope.onNewsSlideChange = function(){
		var index = $ionicSlideBoxDelegate.$getByHandle("news-slider").currentIndex();

		if (index == 0) {
			$timeout(function() {
				enableDashboardScroll();
			}, 500);
		}
		
		disableNewsSlide();
	}

	$scope.onCodesClick = function(){
		$location.path("/app/codes");
		GAService.trackEvent("Coupons", "Dashboard", "clicked", null);
	}

	$scope.onSwipe = function(){
		sliderSwipe = true;
	}
});
