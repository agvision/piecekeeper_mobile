
angular.module('starter')

.controller('ResetCtrl', function($scope, $stateParams, $rootScope, $location, httpService, $stateParams, $ionicPopup, $ionicNavBarDelegate, $ionicSideMenuDelegate) {

	$scope.resetData = {};

	$scope.doReset = function() {
		$scope.resetData.hash = $stateParams.hash;

  		if (angular.isUndefined($scope.resetData.password) || $scope.resetData.password.length == 0) {
  			$ionicPopup.alert({
  	  			title: $rootScope.strings.reset.alertTitleReset,
  	  			template: $rootScope.strings.reset.alertResetErrorPassword
  			});
  		}

  		else if (angular.isUndefined($scope.resetData.confirm_password) || $scope.resetData.password != $scope.resetData.confirm_password) {
  			$ionicPopup.alert({
  	  			title: $rootScope.strings.reset.alertTitleReset,
  	  			template: $rootScope.strings.reset.alertResetErrorPasswordConfirm
  			});	
  		}

  		else {
  			// creating account after all images have been uploaded
  			httpService.resetPassword($scope.resetData, function(data){
  			  	if (data.updated) {

  		    		$ionicPopup.alert({
  			      		title: $rootScope.strings.reset.alertTitleResetSuccess,
  			      		template: $rootScope.strings.reset.alertResetSuccess
  		    		});

  		    		$location.path("/login");
  			    
  			  	} else {
  			    	$scope.showAlert(data.errors);
  			  	}
  			});
  		}
	};

	$scope.showAlert = function(errors){
	  var message = "";

	  for (var i in errors) {
	    message += "<p>" + errors[i] + "</p>";
	  }

	  var alertPopup = $ionicPopup.alert({
	      title: $rootScope.strings.reset.alertTitleReset,
	      template: message
	  });
	}
});
