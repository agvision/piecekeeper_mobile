
angular.module('starter')

.controller('RegisterCtrl', function($scope, $stateParams, $rootScope, $location, httpService, $ionicPopup, $ionicModal, $ionicNavBarDelegate, $ionicSideMenuDelegate, GAService) {
	
	// Get Server Data
	httpService.init("getCountries", "countries");
	httpService.init("getStates", "states");

	$scope.registerData = {};

	$scope.cityInputType = "select";
	$scope.schoolInputType = "select";

	$scope.show_fb_button = true;
	$scope.FBRegistered   = false;

	$scope.upload_message = [];
	$scope.upload_message[0] = $rootScope.strings.register.labelPhotoSelectorTitle;
	$scope.upload_message[1] = $rootScope.strings.register.labelPhotoSelectorTitle;

	$scope.showImageIcon = [];
	$scope.showImageIcon[0] = true;
	$scope.showImageIcon[1] = true;

	$scope.images = [];

	// generate days
	$scope.days = [];
	for (var i = 1; i <= 31; i++) {
	  $scope.days.push(i);
	};

	// generate months
	$scope.months = $rootScope.strings.months;

	// generate years
	$scope.years = [];
	for (var i = 1960; i <= 2010; i++) {
	  $scope.years.push(i);
	};  

	// if invitation is sent, save it on server
	if (angular.isDefined($stateParams.invitation) && $stateParams.invitation.length > 0) {
		httpService.setInvitation({
			invitation: $stateParams.invitation
		});
	}

	$scope.onFacebookClick = function(){
		GAService.trackEvent("Register", "Facebook", "clicked", null);
	  FB.getLoginStatus(function(response){
	    if (response.status != "connected") {
	      FB.login(function(response){
	        if (response.status == "connected") {
	          $scope.getFBProfile();
	        } else {
	          $ionicPopup.alert({
	            title: $rootScope.strings.register.alertTitleFB,
	            template: $rootScope.strings.register.alertErrorFB
	          });
	        }
	      }, {scope: 'public_profile, email, user_birthday, user_education_history, user_location'});
	    } else {
	      $scope.getFBProfile();
	    }
	  });
	}

	$scope.getFBProfile = function(){
		httpService.markRequest("getFBProfile");

	  FB.api("/me", function(response){
	  	GAService.trackEvent("Register", "Facebook", "success", null);

	  	httpService.unmarkRequest("getFBProfile");
	  	
	    if (!response || response.error) {
	      $ionicPopup.alert({
	        title: $rootScope.strings.register.alertTitleFB,
	        template: $rootScope.strings.register.alertErrorFB
	      });
	    } else {
	      $scope.show_fb_button = false;
	      $scope.FBRegistered = true;

	      // get Facebook Profile Picture and upload to Server
	      var FBImageUrl = "http://graph.facebook.com/"+response.id+"/picture?width=900";
	      httpService.uploadUrlProfileImage(1, FBImageUrl);

	      // set proper Image Selector Label
	      $scope.upload_message[0] = $rootScope.strings.register.labelPhotoSelectorTitleFB;
	      $scope.showImageIcon[0]  = false;


	      $scope.registerData.email = response.email;
	      $scope.registerData.first_name = response.first_name;
	      $scope.registerData.last_name = response.last_name;

	      if (response.gender == "male") {
	        $scope.registerData.gender = "1";
	      } else {
	        $scope.registerData.gender = "0";
	      }

	      var data = response.birthday.split("/");

	      if (data.length == 3) {
	        if (data[0][0] == "0") {
	          data[0] = data[0][1];
	        }
	        if (data[1][0] == "0") {
	          data[1] = data[1][1];
	        }

	        $scope.registerData.day = data[1];
	        $scope.registerData.month = data[0];
	        $scope.registerData.year = data[2];
	      }

	      var fb_country = false;
	      var fb_state   = false;
	      var fb_city    = false;

	      // get location
	      if ("location" in response) {
	        data = response.location.name.split(", ");
	        if (data.length == 2) {
	          // insert city
	          $scope.cityInputType = "text";
	          $scope.registerData.city = data[0];
	          fb_city = true;

	          // check if given value is country or state
	          var found = false;
	          for (var i = 0; i < $scope.countries.length; i++) {
	              if (data[1] == $scope.countries[i].short_name) {
	                $scope.registerData.country = $scope.countries[i].country_id + "_" + $scope.countries[i].short_name;
	                fb_country = true;
	                found = true;
	                break;
	              }
	          }

	          if (!found) {
	            for (var i = 0; $scope.states.length; i++) {
	                if (data[1] == $scope.states[i].state) {
	                    $scope.registerData.country = "236_United States";
	                    $scope.registerData.state = $scope.state.state;
	                    break;
	                }
	            }
	          }
	        }
	      }

	      // get school
	      if ("education" in response && response.education.length > 0) {
	          $scope.schoolInputType = "text";
	          $scope.registerData.school = response.education[response.education.length - 1].school.name;
	      } else {
	        if (fb_city) {
	          $scope.onCityChange();
	        }
	        else if (fb_state) {
	          $scope.onStateChange();
	        }
	        else if (fb_country) {
	          $scope.onCountryChange();
	        }
	      }

	      // force view update
	      $scope.$apply();
	    }
	  });
	}

	$scope.doRegister = function() {
		GAService.trackEvent("Register", "PieceKeepers", "clicked", null);

		if (angular.isUndefined($scope.registerData.password) || $scope.registerData.password.length == 0) {
			$ionicPopup.alert({
	  			title: $rootScope.strings.register.alertTitleRegister,
	  			template: $rootScope.strings.register.alertRegisterErrorPassword
			});
		}

		else if (angular.isUndefined($scope.registerData.confirm_password) || $scope.registerData.password != $scope.registerData.confirm_password) {
			$ionicPopup.alert({
	  			title: $rootScope.strings.register.alertTitleRegister,
	  			template: $rootScope.strings.register.alertRegisterErrorPasswordConfirm
			});	
		}

		else {
			// creating account after all images have been uploaded
			httpService.register($scope.registerData, function(data){
			  	if (data.registered) {

			  		GAService.trackEvent("Register", "PieceKeepers", "success", null);

		    		$location.path("/social");
			    
			  	} else {
			    	$scope.showAlert(data.errors);
			  	}
			});
		}

	};

	$scope.onCountryChange = function() {
	  var data = $scope.registerData.country.split("_");

	  $scope.cityInputType = "select";
	  $scope.schoolInputType = "select";
	  $scope.registerData.city = "";
	  $scope.registerData.school = "";
	  
	  httpService.getCities({
	    country : data[0]
	  });

	  httpService.getSchools({
	    country : data[0]
	  });
	}

	$scope.onStateChange = function() {
	  var data = $scope.registerData.country.split("_");
	  
	  $scope.cityInputType = "select";
	  $scope.schoolInputType = "select";
	  $scope.registerData.city = "";
	  $scope.registerData.school = "";

	  httpService.getCities({
	    country : data[0],
	    state : $scope.registerData.state
	  });
	}

	$scope.onCityChange = function() {
	  var data = $scope.registerData.country.split("_");
	  var city = $scope.registerData.city;

	    $scope.schoolInputType = "select";
	    $scope.registerData.school = "";

	  if (city == "-1") {
	    $scope.cityInputType = "text";
	    $scope.registerData.city = "";
	  }

	  httpService.getSchools({
	    country : data[0],
	    city : city
	  });
	}

	$scope.onSchoolChange = function() {
	  var school = $scope.registerData.school;

	  if (school == "-1") {
	    $scope.schoolInputType = "text";
	    $scope.registerData.school = "";
	  }
	}

	$scope.onImageWrapperClick = function(selector){
		// if FBRegistered use FB Profile Pic
		if (parseInt(selector) == 0 && $scope.FBRegistered) {
			$ionicPopup.alert({
			    title: $rootScope.strings.register.alertTitleImage,
			    template: $rootScope.strings.register.alertImageFBUploaded
			});
		}
	}

	$scope.onFileSelected = function(selector, file) {

		// refresh image model
		delete $scope.images[selector];

	  	var data = file.value.split("\\");
	  	$scope.upload_message[selector] = data[data.length-1];
	  	$scope.showImageIcon[selector]  = false; 

	  	if ($scope.upload_message[0] == $scope.upload_message[1]) {
	      	var name = $scope.upload_message[0];
	      	var data = name.split(".");
	      	var ext  = data[data.length-1];

	      	$scope.upload_message[0] = name.replace("."+ext, " 1."+ext); 
	      	$scope.upload_message[1] = name.replace("."+ext, " 2."+ext); 
	  	}

	  	var unregister = $scope.$watch("images["+selector+"]", function(value){
	  		if (angular.isDefined($scope.images[selector])) {	
	  			unregister();			

	  			if ($scope.images[selector].size > 2000000) {
	  				$ionicPopup.alert({
  					    title: $rootScope.strings.register.alertTitleRegister,
  					    template: $rootScope.strings.register.alertRegisterErrorPhotoSize
  					});

  					delete $scope.images[selector];
  					$scope.upload_message[selector] = $rootScope.strings.register.labelPhotoSelectorTitle;
  					$scope.showImageIcon[selector]  = true;
	  			} else {
	  				
	  				// send image to server
	  				
	  				httpService.uploadProfileImage(selector+1, $scope.images[selector], function(data){
	  					
	  				});
	  			}
	  		}
	  	});

	  	$scope.$apply();
	}

	$scope.showAlert = function(errors){
	  var message = "";

	  for (var i in errors) {
	    message += "<p>" + errors[i] + "</p>";
	  }

	  var alertPopup = $ionicPopup.alert({
	      title: $rootScope.strings.register.alertTitleRegister,
	      template: message
	  });
	}
});
