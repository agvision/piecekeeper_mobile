
angular.module('starter')

.controller('ProfileCtrl', function($scope, $rootScope, $stateParams, $ionicPopup, $window, $timeout, localService, httpService) {

	// Init Server Data
	httpService.init("getProfile", "profile", "logged_in");
	httpService.init("getCountries", "countries");
	httpService.init("getStates", "states");
	httpService.init("getSocialNetworks", "networks", "logged_in");

	$scope.profileData = {};

	// Init city and school input type as text
	$scope.cityInputType = "text";
	$scope.schoolInputType = "text";

	// generate days
	$scope.days = [];
	for (var i = 1; i <= 31; i++) {
	  $scope.days.push(i);
	};

	// generate months
	$scope.months = $rootScope.strings.months;

	// generate years
	$scope.years = [];
	for (var i = 1960; i <= 2010; i++) {
	  $scope.years.push(i);
	};  

	localService.get("profile", function(profile){
		// use timeout to force view loading before running this code
		$timeout(function(){
			$scope.profileData = profile;
		});
	});

	// Country changed listener
	$scope.onCountryChange = function() {
	  var country = $scope.profileData.country.id;

	  $scope.cityInputType = "select";
	  $scope.schoolInputType = "select";
	  $scope.profileData.city = "";
	  $scope.profileData.school = "";
	  
	  // update cities and schools based on country
	  httpService.getCities({
	    country : country
	  });

	  httpService.getSchools({
	    country : country
	  });
	}

	$scope.onStateChange = function() {
	  
	  $scope.cityInputType = "select";
	  $scope.schoolInputType = "select";
	  $scope.profileData.city = "";
	  $scope.profileData.school = "";

	  httpService.getCities({
	    country : $scope.profileData.country.id,
	    state : $scope.profileData.state
	  });
	}

	$scope.onCityChange = function() {
	  var country = $scope.profileData.country.id;
	  var city    = $scope.profileData.city;

	    $scope.schoolInputType = "select";
	    $scope.profileData.school = "";

	  if (city == "-1") {
	    $scope.cityInputType = "text";
	    $scope.profileData.city = "";
	  }

	  httpService.getSchools({
	    country : country,
	    city : city
	  });
	}

	$scope.onSchoolChange = function() {
	  var school = $scope.profileData.school;

	  if (school == "-1") {
	    $scope.schoolInputType = "text";
	    $scope.profileData.school = "";
	  }
	}

	$scope.updateProfile = function(){

		$ionicPopup.confirm({
			title: $rootScope.strings.profile.confirmTitleUpdate,
			template: $rootScope.strings.profile.confirmUpdate
		}).then(function(confirm){
			if (confirm) {

				// init data
				var country = $rootScope.profile.country.name;
				var id_country = $rootScope.profile.country.id;
				var school = $rootScope.profile.school.name;
				var graduation_year = $rootScope.profile.school.graduation_year;

				$scope.profileData.country = country;
				$scope.profileData.id_country = id_country;
				$scope.profileData.school = school;
				$scope.profileData.graduation_year = graduation_year;

				// send information to server
				httpService.updateProfile($scope.profileData, function(data){
					if (data.updated) { 
						httpService.getProfile(function(data){
							$scope.profileData = data;

							$ionicPopup.alert({
							  title: $rootScope.strings.profile.alertTitleUpdate,
							  template: $rootScope.strings.profile.alertUpdateSuccess
							}); 
						});
					}
				}, function(data){
					var errors = data.errors;
					httpService.getProfile(function(data){
						$scope.profileData = data;

						$scope.showAlert($rootScope.strings.profile.alertTitleUpdate, errors);
					});
				});
			}
		});
	}
});
