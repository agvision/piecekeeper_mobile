
angular.module('starter')

// directive for goals accordion reposition after toggle goal
.directive("goalsRepeat", function($ionicScrollDelegate, $timeout){
	return function(scope, elements, attrs){
		scope.$watch(function(){
			return elements;
		}, function(elements){

			for (var i = elements.length - 1; i >= 0; i--) {
				var el = elements[i]

				var value = el.lastElementChild.className;
				if (value == "item-accordion item-text-wrap item") {
					$ionicScrollDelegate.resize();
					$ionicScrollDelegate.scrollTop(true);
				}

				var item_accordion = el.childNodes[3];

				angular.element(item_accordion).bind("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", function(event){

				});
			};

		});
	}
})

.controller('GoalsCtrl', function($scope, $rootScope, $stateParams, $timeout, $ionicScrollDelegate, localService, httpService) {

	// Init Server Data
	httpService.init("getGoals", "goals", "logged_in");

	var current_active = 0;

	localService.get("goals", function(goals){

		// compute goals details
		for (var i in goals) {
			goals[i].index = parseInt(i);
			goals[i].arrow_type = "ion-arrow-right-b";

			goals[i].deadline = goals[i].deadline.toString() + "000"; 

			if (goals[i].rank_status_class == "rank-bottom-success") {
				goals[i].rank_class = "success";
			} else {
				goals[i].rank_class = "error";
			}
		}

		$rootScope.goals = goals;

		/*
		 * show first goal
		 */
		$scope.shownGoal = $scope.goals[0];
		$scope.goals[0].arrow_type = "ion-arrow-down-b";
	});

	/*
	 * open a goal
	 */
	$scope.toggleGoal = function(index) {
		if (index == current_active) {
			return;
		}

		// get info tag height
		var info_tag = angular.element(document.getElementById("goals-info"));
		var offsetY = info_tag[0].offsetHeight;

		// get height of every item above current one
		for (i = 0; i < index; i++) {
			var el = angular.element(document.getElementById("item-"+i));
			offsetY += el[0].offsetHeight;
		}

		$ionicScrollDelegate.$getByHandle("goals").resize();
		$ionicScrollDelegate.$getByHandle("goals").scrollTo(0, offsetY, true);

	    $scope.shownGoal = $scope.goals[index];
	    $scope.goals[index].arrow_type = "ion-arrow-down-b";
	    $scope.goals[current_active].arrow_type = "ion-arrow-right-b";

	    current_active = index;
	};

	$scope.isGoalShown = function(index) {
	   	return $scope.shownGoal === $scope.goals[index];
	};

});
