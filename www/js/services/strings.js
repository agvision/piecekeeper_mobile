
angular.module('starter')

.factory('StringsService', function($window, $rootScope, $location) {
	var StringsService = {};

	StringsService.getStrings = function(page){
		if (angular.isDefined(page) && page.length > 0 && angular.isDefined(this.strings[page])) {
			return this.strings[page];
		}

		return this.strings;
	}

	StringsService.strings = {
		// Months
		months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],

		// Menu Section
		menu: {
			sectionTitle: "The PieceKeepers",

			itemDashboard: "Dashboard",
			itemProfile: "Profile",
			itemCodes: "Discount Codes",
			itemTeam: "Sales Team",
			itemRanking: "Ranking",
			itemLevels: "Levels",
			itemGoals: "Goals",
			itemMissions: "Missions",
			itemPayouts: "Payouts",
			itemLogout: "Logout"
		},

		// Dashboard Page
		dashboard: {
			pageTitle: "Dashboard",

			labelCreateCode: "Create a promo code",
			labelEarningsCash: "Earnings Cash",
			labelEarningsGiftcard: "Earnings Giftcard",
			labelGlobalRanking: "Global Ranking",
			labelTotalPoints: "Total Points",
			labelBonusPoints: "Bonus",
			labelSalesPoints: "Sales",
			labelLevel: "Level",
			labelSales: "Sales",
			labelBlog: "Blog",
			labelLatestPosts: "Latest Posts"
		},

		// Codes Page
		codes: {
			pageInfo: "You can create discount codes to send to a friend or use them on social media networks.",
			pageTitle: "Discount Codes",

			alertTitle: "Discount Code",

			inputCodeInfo: "Your Code (letters and numbers)",
			inputLinkInfo: "Link to where you will post you code",

			labelMultiuseType: "Multi-use",
			labelSingleuseType: "Single-use",
			labelCreatedSuccess: "Your code has been created.",
			labelRequestTitle: "Discount Code",

			actionRequest: "Create",

			codesTableTitle: "Discount Codes",
			codesTableHeaderCode: "Code",
			codesTableHeaderDiscount: "Discount",
			codesTableHeaderExpires: "Expires",
			codesTableNoData: "Start now and create some discount codes!"
		},

		// Goals Page
		goals: {
			pageTitle: "Goals",
			pageInfo: "Goals will be based on PieceKeepers reaching a certain number of points.",

			labelPoints: "Points required:",
			labelInvited: "Who will be invited:",
			labelDeadline: "Deadline:",
			labelTop: "Top",
			labelPieceKeepers: "PieceKeepers"
		},

		// Level Page
		levels: {
			pageTitle: "Levels",

			labelLevel1: "Level 1",
			labelLevel2: "Level 2",
			labelLevel3: "Level 3",
			labelCurrentLevel: "your current level",
			labelCommission: "commission on all sales"
		},

		// Forgot Password Page
		forgot: {
			alertTitle: "Recover Password",
			alertSentSuccess: "You will receive an email notification.",
			alertSentErrorEmail: "You have to enter a valid email address.",

			inputEmailInfo: "Email Address",

			actionSend: "Recover",
		},

		// Login Page
		login: {
			alertTitle: "Login",
			alertTitleFB: "Facebook Login",
			alertErrorInvalidEmail: "Invalid email or password.",
			alertErrorBlocked: "Your IP address has been blocked for 24 hours.",
			alertFBError: "Something went wrong.",

			labelForgotPassword: "Forgot Password?",
			labelRegister: "Register",

			inputEmailInfo: "Email Address",
			inputPasswordInfo: "Password",

			actionLogin: "Log in",
			actionFBLogin: "Login with Facebook"
		},

		// Missions Page
		missions: {
			pageTitle: "Missions",

			alertTitleSubmitted: "Mission Submitted",
			alertTitleUnsubmitted: "Mission Not Submitted",
			alertTitleShared: "Share Completed",
			alertSubmittedSuccess: "Please wait for admin approval.",
			alertSharedFBSuccess: "You've successfully shared this mission on Facebook.",
			alertSharedTWSuccess: "You've successfully shared this mission on Twitter.",

			labelPoints: "Points",
			labelFBShare: "Share on Facebook",
			labelTWShare: "Share on Twitter",
			labelPINShare: "Share on Pinterest",
			labelUploadPhoto: "Upload Photo",
			labelIncompleted: "Incompleted",
			labelWaiting: "Waiting",
			labelCompleted: "Completed",

			actionSend: "Send"
		},

		// Payouts Page
		payouts: {
			pageTitle: "Payouts",
			pageInfo: "There are restrictions on how much you can earn as a PieceKeeper and this varies from country to country. Check <a href='#/terms'>Terms and Conditions</a> for more information.",

			labelStatusRejected: "Rejected",
			labelStatusWaiting: "Waiting",
			labelStatusApproved: "Approved",
			labelCommission: "Your commission:",
			labelGiftcardPre: "as",
			labelGiftcard: "Giftcard",
			labelGiftcardExtra: "(Get 200% Extra)",
			labelPayPalPre: "to",
			labelPayPal: "Pay to PayPal account",

			alertTitleRequest: "Request Payment",
			alertTitlePayPal: "PayPal",
			alertRequestSuccessGiftcard: "Your giftcard will be sent by email.",
			alertRequestSuccessPayPal: "Please wait for admin approval.",
			alertRequestErrorPayment: "Please choose a payment method.",
			alertRequestErrorPayPal: "Please set your PayPal email.",
			alertPayPalSuccess: "Your PayPal email address has been updated.",

			confirmTitlePayment: "Request Payment",
			confirmTitlePayPal: "PayPal",
			confirmPaymentGiftcard: "Are you sure you want this payment as a Giftcard?",
			confirmPaymentPayPal: "Are you sure you want this payment to your PayPal account?",
			confirmPayPal: "Are you sure you want to change your PayPal email address?",

			inputPayPalInfo: "Your PayPal Email",

			actionUpdatePayPal: "Update PayPal Email",
			actionRequest: "Request Payment",

			paymentsTableTitle: "Payments",
			paymentsTableHeaderAmount: "Amount",
			paymentsTableHeaderStatus: "Status",
			paymentsTableHeaderDate: "Date",
			paymentsTableNoData: "You don't have any payments yet.",

			giftcardsTableTitle: "Giftcards",
			giftcardsTableHeaderCode: "Code",
			giftcardsTableHeaderDiscount: "Discount",
			giftcardsTableNoData: "You don't have any giftcards yet."
		},

		// Profile Page
		profile: {
			pageTitle: "Profile",
			pageInfo: "View and edit information about your profile and social networks connections.",

			confirmTitleUpdate: "Update Profile",
			confirmUpdate: "Are you sure you want to update your profile?",

			alertTitleUpdate: "Update Profile",
			alertUpdateSuccess: "Your profile has been updated.",

			inputFirstNameInfo: "First Name",
			inputLastNameInfo: "Last Name",
			inputEmailInfo: "Your Email",
			inputGenderInfo: "Gender",
			inputPhoneInfo: "Phone",
			inputBirthdayInfo: "Birthday",
			inputAddress1Info: "Address 1",
			inputAddress2Info: "Address 2",
			inputCodeInfo: "Zip/Postal Code",
			inputCityInfo: "City",
			inputSchoolInfo: "School",
			inputGraduationInfo: "Graduation Year",

			labelMale: "Male",
			labelFemale: "Female",
			labelDay: "Day",
			labelMonth: "Month",
			labelYear: "Year",
			labelAddress: "Address",
			labelCountry: "Country",
			labelState: "State",
			labelCity: "City",
			labelSchool: "School",
			labelOther: "Other",

			actionUpdate: "Update Profile",
			actionEditNetworks: "Edit Social Networks"
		},

		// Ranking Page
		ranking: {
			pageTitle: "Ranking",
			pageInfo: "Your ranking shows how you are doing compared to other piecekeepers.",

			statusLastMonth: "Last Month",
			statusSameLastMonth: "Same as Last Month",

			labelGlobal: "Global",
			labelCountry: "Country",
			labelCity: "City",

			topTableTitle: "Top 20 Global Piecekeepers",
			topTableHeaderName: "Name",
			topTableHeaderCountry: "Country",
			topTableHeaderPoints: "Total Pts"

		},

		// Register Page
		register: {
			pageInfo: "Please fill out the application form below. You will receive an email once we've reviewed it.",

			alertTitleFB: "Facebook Login",
			alertTitleRegister: "Register",
			alertTitleImage: "Upload Image",
			alertFBError: "Something went wrong.",
			alertRegisterErrorPassword: "Please choose a password.",
			alertRegisterErrorPasswordConfirm: "Password confirmation doesn't match your password.",
			alertRegisterErrorPhotoSize: "Your photo has reached the maximum size of 2MB.",
			alertImageFBUploaded: "Your Facebook profile image is already uploaded.",

			inputEmailInfo: "Email",
			inputPasswordInfo: "Password",
			inputConfirmPasswordInfo: "Confirm Password",
			inputFirstNameInfo: "First Name",
			inputLastNameInfo: "Last Name",
			inputGenderInfo: "Gender",
			inputPhoneInfo: "Phone",
			inputBirthdayInfo: "Birthday",
			inputAddress1Info: "Address line 1",
			inputAddress2Info: "Address line 2",
			inputCodeInfo: "Zip/Postal Code",
			inputCityInfo: "City",
			inputSchoolInfo: "School",
			inputGraduationInfo: "Graduation Year",

			labelPhotoSelectorTitle: "Choose Photo",
			labelPhotoSelectorTitleFB: "Facebook Profile Image",
			labelRegisterFB: "Register with Facebook",
			labelImages: "Please upload 2 profile images",
			labelMale: "Male",
			labelFemale: "Female",
			labelDay: "Day",
			labelMonth: "Month",
			labelYear: "Year",
			labelAddress: "Address",
			labelCountry: "Country",
			labelState: "State",
			labelCity: "City",
			labelSchool: "School",
			labelOther: "Other",
			labelAccept: "I accept",
			labelTerms: "terms and conditions",

			actionSubmit: "Next"
		},

		// Reset Password Page
		reset: {
			alertTitleReset: "Reset Password",
			alertTitleesResetSuccess: "Reset Completed",
			alertResetErrorPassword: "Please choose a new password.",
			alertResetErrorPasswordConfirm: "Password confirmation doesn't match New Password.",
			alertResetSuccess: "You can now login to your account.",

			inputNewPasswordInfo: "New Password",
			inputConfirmPasswordInfo: "Confirm Password",

			actionReset: "Reset"
		},

		// Sales Team Page
		salesteam: {
			pageTitle: "Sales Team",
			pageInfo: "Since you have reached level 3 of the PieceKeeper program you have proven to us that you are a great ambassador. Now you have the chance to create a sales team to work for you.",

			labelStatusAccepted: "Accepted",
			labelStatusPending: "Pending",
			labelInvite: "You can invite",
			labelUnderKeepers: "UnderKeepers",
			labelInvitations: "invitations left",

			inputUnderKeeperInfo: "UnderKeeper Email",

			alertTitleInvite: "Invite UnderKeeper",
			alertInviteErrorEmail: "Please enter an email address.",
			alertInviteSuccess: "Your invitation has been sent.",

			confirmTitleInvite: "Invite UnderKeeper",
			confirmInvite: "Are your sure you want to send this invitation?",

			underKeepersTableTitle: "UnderKeepers",
			underKeepersTableHeaderEmail: "Email",
			underKeepersTableHeaderStatus: "Status",
			underKeepersTableNoData: "Start now and build your team!",

			actionInvite: "Invite"
		},

		// Social Linking Page
		social: {
			pageTitleProfile: "Social Networks",
			pageInfoProfile: "Change and update your social networks accounts.",
			pageInfo: "Please link up your social media profile below. We <b>ONLY</b> pull your following count so we can determine your social reach. The more accounts you enter the bigger chance of being accepted. <br/><br/> <b>You have to enter at least one social account.</b>",
			
			alertTitleChooseFBAccount: "Choose Account",
			alertTitleRegisterSuccess: "Register Completed",
			alertTitleRegister: "Register",
			alertTitleLink: "Link Social Network",
			alertTitleFB: "Facebook Linking",
			alertTitleTW: "Twitter Linking",
			alertTitleINST: "Instagram Linking",
			alertTitleTUB: "Tumblr Linking",
			alertTitlePIN: "Pinterest Linking",
			alertTitleYB: "Youtube Linking",
			alertTitleVN: "Vine Linking",
			alertLinkError: "We couldn't link this social network. Please try again.",
			alertLinkErrorLinked: "This social account has already been linked.",
			alertLinkErrorTW: "We couldn't link Twitter. Please try again.",
			alertLinkErrorFB: "Something went wrong.",
			alertLinkErrorPIN: "This username is invalid.",
			alertLinkErrorVN: "This user ID is invalid.",
			alertRegisterErrorUser: "Something went wrong. Please try again.",
			alertRegisterErrorNetwork: "You need to link at least one social network.",
			alertRegisterSuccess: "Please wait for admin approval.",

			popupTitlePIN: "Enter your Pinterest username",
			popupTitleVN:  "Enter your Vine user ID",

			actionLink: "Link",
			actionFBLink: "Link Facebook",
			actionTWLink: "Link Twitter",
			actionINSTLink: "Link Instagram",
			actionTUBLink: "Link Tumblr",
			actionPINLink: "Link Pinterest",
			actionYBLink: "Link Youtube",
			actionVNLink: "Link Vine",
			actionSubmit: "Submit"
		},

		terms: {
			pageTitle: "Terms and Conditions"
		}
	}

	return StringsService;
});
