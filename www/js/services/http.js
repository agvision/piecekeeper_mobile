
angular.module('starter')

.factory('httpService', function($rootScope, $http, $ionicLoading, GAService, $interval, $location) {
	var httpService = {};

	httpService.active_requests = 0;
	httpService.counters = {};

	$rootScope.auth_key   = "193575f7a22e0384ce041b7ecceaaed06c7fcfd5"; // production
	// $rootScope.auth_key   = "9ca6b938f38401bc21c3302e519e67c02ea7f753"; // development
	// $rootScope.server_url = "http://localhost/piecekeeper/";
	$rootScope.server_url = "https://piecekeeper.onepiece.com/";

	httpService.markRequest = function(action){
		if (this.active_requests == 0) {
			httpService.counters["total"] = httpService.setTimeCounter();
			$ionicLoading.show();
		}

		this.active_requests++;

		httpService.counters[action] = httpService.setTimeCounter();
	}

	httpService.unmarkRequest = function(action){
		if (this.active_requests > 0) {
			this.active_requests--;
		}

		if (this.active_requests == 0) {

			if (angular.isDefined(httpService.counters["total"])) {
				var time = httpService.counters["total"].getTime();
				GAService.trackRequestTime("total", time);
			}

			$ionicLoading.hide();
		}

		if (angular.isDefined(httpService.counters[action])) {
			var time = httpService.counters[action].getTime();
			GAService.trackRequestTime(action, time);
		}
	}

	httpService.setTimeCounter = function(action){
		var counter = {};

		counter.value = 0;

		counter.count = function(){
			return $interval(function(){
				counter.value ++;
			}, 100);
		}

		counter.trigger = counter.count();

		counter.getTime = function(){
			GAService.trackRequestTime(action, counter.value);
			$interval.cancel(counter.trigger);
			return counter.value;
		}

		return counter;
	}

	httpService.authNetworkRequest = function(authData, success, error){
		httpService.markRequest("authNetworkRequest");
		var path = "";

		// build request Url based on oAuth step
		if (authData.step == "redirectUrl") {
			path = authData.step+"?key="+$rootScope.auth_key;
		} else if (authData.step == "getUser") {
			// build Request Url based on network
			if (authData.service == "twitter" || authData.service == "tumblr") {
				path = authData.step+"?key="+$rootScope.auth_key+"&oauth_token="+authData.oauth_token+"&oauth_verifier="+authData.oauth_verifier;
			} else if (authData.service == "instagram") {
				path = authData.step+"?key="+$rootScope.auth_key+"&code="+authData.code;
			} else if (authData.service == "pinterest") {
				path = authData.step+"?key="+$rootScope.auth_key+"&username="+authData.username;
			} else if (authData.service == "youtube") {
				path = authData.step+"?key="+$rootScope.auth_key+"&state="+authData.state+"&code="+authData.code;
			} else if (authData.service == "vine") {
				path = authData.step+"?key="+$rootScope.auth_key+"&userID="+authData.userID;
			}
		}

		// set redirect page
		if ($location.path().match(/profile/)) {
			path += "&redirectUrl=profile";
		} else {
			path += "&redirectUrl=social"
		}

		$http.get($rootScope.server_url+"api/auth/"+authData.service+"/"+path)
			.success(function(data){
				httpService.unmarkRequest("authNetworkRequest");

			  	if (angular.isFunction(success)) {
			  		success(data);
			  	}
			})
			.error(function(data){
				httpService.unmarkRequest("authNetworkRequest");

			  	if (angular.isFunction(error)) {
			  		error(data);
			  	}
			});
	}

	httpService.uploadProfileImage = function(index, file, success){

        var fd = new FormData();
        fd.append('file', file);
        $http.post($rootScope.server_url+"api/profile_image/"+index+"?key="+$rootScope.auth_key, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(data){
        	httpService.unmarkRequest("uploadProfileImage");

        	if (angular.isFunction(success)) {
        		success(data);
        	}
        })
        .error(function(){
        });
    }

	httpService.uploadUrlProfileImage = function(index, url, success, error){

        $http.post($rootScope.server_url+"api/profile_image/"+index+"?key="+$rootScope.auth_key, {
        	imgUrl: url
        })
        .success(function(data){
        	httpService.unmarkRequest("uploadProfileImage");

        	if (angular.isFunction(success)) {
        		success(data);
        	}
        })
        .error(function(){
        	httpService.unmarkRequest("uploadProfileImage");

        	if (angular.isFunction(error)) {
        		error(data);
        	}
        });
    }

    httpService.uploadMissionImage = function(id_mission, file, success){
    	httpService.markRequest("uploadMissionImage");

        var fd = new FormData();
        fd.append('file', file);
        $http.post($rootScope.server_url+"api2/missions/"+id_mission+"/image?key="+$rootScope.auth_key, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(data){
        	httpService.unmarkRequest("uploadMissionImage");

        	if (angular.isFunction(success)) {
        		success(data);
        	}
        })
        .error(function(){
        });
    }

	httpService.checkLogin = function(success, error){
		httpService.markRequest("checkLogin");

		$http.get($rootScope.server_url+"api2/isLogged?key="+$rootScope.auth_key).success(function(data){
			httpService.unmarkRequest("checkLogin");

		  	if(!data.logged){
		  		$rootScope.logged_in = false;
		  		if (angular.isFunction(error)) {
		  			error();
		  		}
		  	} else {
		    	$rootScope.$broadcast("logged_in");
		    	$rootScope.logged_in = true;
		    	if (angular.isFunction(success)) {
		    		success();
		    	}
		  	}
		});
	}

	httpService.login = function(loginData, success, error){
		httpService.markRequest("login");

		$http.get($rootScope.server_url+"api2/login?key="+$rootScope.auth_key+"&email="+loginData.email+"&password="+loginData.password)
		  	.success(function(data){
		  		httpService.unmarkRequest("login");

		    	if (data.logged) {
		      		$rootScope.$broadcast("logged_in");
		      		$rootScope.logged_in = true;
		      		if (angular.isFunction(success)) {
		      			success();
		      		}
		    	} else {
		    		if (angular.isFunction(error)) {
		    			error("invalid");
		    		}
		    	}
			})
			.error(function(data, status){
				httpService.unmarkRequest("login");

				if (status == 403) {
					if (angular.isFunction(error)) {
						error("blocked");
					}
				}
			});
	}

	httpService.FBLogin = function(loginData, success, error){
		httpService.markRequest("FBLogin");

		$http.post($rootScope.server_url+"api2/FBLogin?key="+$rootScope.auth_key, loginData)
			.success(function(data){
				httpService.unmarkRequest("FBLogin");

				$rootScope.$broadcast("logged_in");
				$rootScope.logged_in = true;

				if (angular.isFunction(success)) {
					success(data);
				}
			})
			.error(function(data){
				httpService.unmarkRequest("FBLogin");

				if (angular.isFunction(error)) {
					error(data);
				}
			});
	}

	httpService.linkSocialNetwork = function(networkData, success, error){
		httpService.markRequest("linkNetwork");

		$http.post($rootScope.server_url+"api2/linkNetwork?key="+$rootScope.auth_key, networkData)
			.success(function(data){
				httpService.unmarkRequest("linkNetwork");

				if (angular.isFunction(success)) {
					success(data);
				}
			})
			.error(function(data){
				httpService.unmarkRequest("linkNetwork");

				if (angular.isFunction(error)) {
					error(data);
				}
			});
	}

	httpService.register = function(registerData, callback){
		httpService.markRequest("register");

		$http.post($rootScope.server_url+"api/register?key="+$rootScope.auth_key, registerData)
		  	.success(function(data){
		  		httpService.unmarkRequest("register");

	      		if (angular.isFunction(callback)) {
	      			callback(data);
	      		}
			});
	}

	httpService.completeRegistration = function(success, error){
		httpService.markRequest("completeRegistration");

		$http.get($rootScope.server_url+"api/completeRegistration?key="+$rootScope.auth_key)
		  	.success(function(data){
		  		httpService.unmarkRequest("completeRegistration");

	      		if (angular.isFunction(success)) {
	      			success(data);
	      		}
			})
			.error(function(data){
		  		httpService.unmarkRequest("completeRegistration");

	      		if (angular.isFunction(error)) {
	      			error(data);
	      		}
			});
	}

	httpService.logout = function(callback){
		httpService.markRequest("logout");

		$http.get($rootScope.server_url+"api2/logout?key="+$rootScope.auth_key).success(function(data){
		  	httpService.unmarkRequest("logout");

		  	if (data.logout) {
		  		$rootScope.logged_in = false;
		  		if (angular.isFunction(callback)) {
		  			callback();
		  		}
		  	}
		});
	}

	httpService.getProfile = function(callback){
		httpService.markRequest("getProfile");

      	$http.get($rootScope.server_url+"api/profile?key="+$rootScope.auth_key).success(function(data){
        	httpService.unmarkRequest("getProfile");
        	$rootScope.profile = data;
        	$rootScope.$broadcast("profile");
        	if (angular.isFunction(callback)) {
        		callback(data);
        	}
      	});
	}

	httpService.getSocialNetworks = function(success, error){
		httpService.markRequest("getSocialNetworks");

      	$http.get($rootScope.server_url+"api/socialNetworks?key="+$rootScope.auth_key)
	      	.success(function(data){
	        	httpService.unmarkRequest("getSocialNetworks");
	        	$rootScope.networks = data.networks;

	        	if (angular.isFunction(success)) {
	        		success(data);
	        	}
	      	})
	      	.error(function(data){
	      		httpService.unmarkRequest("getSocialNetworks");

	      		if (angular.isFunction(error)) {
	      			error(data);
	      		}
	      	});
	}

	httpService.getSalesTeam = function(callback){
		httpService.markRequest("getSalesTeam");

      	$http.get($rootScope.server_url+"api2/sales_team?key="+$rootScope.auth_key).success(function(data){
        	httpService.unmarkRequest("getSalesTeam");
        	$rootScope.salesteam = data;
        	if (angular.isFunction(callback)) {
        		callback(data);
        	}
      	});
	}

	httpService.sendTeamInvitation = function(invitationData, success, error){
		httpService.markRequest("sendTeamInvitation");

      	$http.post($rootScope.server_url+"api2/sales_team/invite?key="+$rootScope.auth_key, invitationData)
      	.success(function(data){
        	httpService.unmarkRequest("sendTeamInvitation");
        	if (angular.isFunction(success)) {
        		success(data);
        	}
      	})
      	.error(function(data){
        	httpService.unmarkRequest("sendTeamInvitation");
        	if (angular.isFunction(error)) {
        		error(data);
        	}
      	});
	}

	httpService.getLevels = function(){
		httpService.markRequest("getLevels");

		$http.get($rootScope.server_url+"api2/levels?key="+$rootScope.auth_key).success(function(data){
		  	httpService.unmarkRequest("getLevels");
		  	$rootScope.levels = data;
		});
	}

	httpService.getGlobalRanking = function(){
		httpService.markRequest("getGlobalRanking");

		$http.get($rootScope.server_url+"api/ranking?key="+$rootScope.auth_key+"&length=20").success(function(data){
		  	httpService.unmarkRequest("getGlobalRanking");
		  	$rootScope.globalRanking = data;
		});
	}

	httpService.getGoals = function(){
		httpService.markRequest("getGoals");

		$http.get($rootScope.server_url+"api/goals?key="+$rootScope.auth_key).success(function(data){
		  	httpService.unmarkRequest("getGoals");
		  	$rootScope.goals = data;
		});
	}

	httpService.getDiscountCodes = function(){
		httpService.markRequest("getDiscountCodes");

		$http.get($rootScope.server_url+"api2/discountCode?key="+$rootScope.auth_key).success(function(data){
		  	httpService.unmarkRequest("getDiscountCodes");
		  	$rootScope.discountCodes = data.discount_codes;
		});
	}

	httpService.getMissions = function(callback){
		httpService.markRequest("getMissions");

		$http.get($rootScope.server_url+"api2/missions?key="+$rootScope.auth_key).success(function(data){
			httpService.unmarkRequest("getMissions");

		  	$rootScope.missions = data.missions;
		  	if (angular.isFunction(callback)) {
		  		callback(data);
		  	}
		});
	}

	httpService.getCountries = function(){
		httpService.markRequest("getCountries");

		$http.get($rootScope.server_url+"api/countries?key="+$rootScope.auth_key).success(function(data){
		  	httpService.unmarkRequest("getCountries");
		  	$rootScope.countries = data.countries;
		});
	}

	httpService.getStates = function(){
		httpService.markRequest("getStates");

		$http.get($rootScope.server_url+"api2/states?key="+$rootScope.auth_key).success(function(data){
		  	httpService.unmarkRequest("getStates");
		  	$rootScope.states = data.states;
		});
	}

	httpService.getCities = function(citiesData, callback){
		httpService.markRequest("getCities");

		$http.get($rootScope.server_url+"api/cities?key="+$rootScope.auth_key+"&country="+citiesData.country+"&state="+citiesData.state).success(function(data){
		  	httpService.unmarkRequest("getCities");
		  	$rootScope.cities = data.cities;

		  	if (angular.isFunction(callback)) {
		  		callback(data);
		  	}
		});
	}

	httpService.getSchools = function(schoolsData){
		httpService.markRequest("getSchools");

		$http.get($rootScope.server_url+"api2/schools?key="+$rootScope.auth_key+"&id_country="+schoolsData.country+"&state="+schoolsData.state+"&city="+schoolsData.city).success(function(data){
		  	httpService.unmarkRequest("getSchools");
		  	$rootScope.schools = data.schools;
		});
	}

	httpService.getBlogs = function(latest){
		httpService.markRequest("getBlogs");

		$http.get($rootScope.server_url+"api2/blogs/latest/"+latest+"?key="+$rootScope.auth_key).success(function(data){
		  	httpService.unmarkRequest("getBlogs");
		  	$rootScope.blogs = data.blogs;
		});
	}

	httpService.getPayouts = function(){
		httpService.markRequest("getPayouts");

		$http.get($rootScope.server_url+"api2/payouts?key="+$rootScope.auth_key).success(function(data){
		  	httpService.unmarkRequest("getPayouts");
		  	$rootScope.payouts = data.payouts;
		});
	}

	httpService.getTerms = function(){
		httpService.markRequest("getTerms");

		$http.get($rootScope.server_url+"api2/terms?key="+$rootScope.auth_key).success(function(data){
		  	httpService.unmarkRequest("getTerms");
		  	$rootScope.terms = data.terms;
		});
	}

	httpService.requestPayment = function(paymentData, success, error){
		httpService.markRequest("requestPayment");

		$http.post($rootScope.server_url+"api2/payouts/requestPayment?key="+$rootScope.auth_key, paymentData)
		.success(function(data){
		  	httpService.unmarkRequest("requestPayment");
		  	if (angular.isFunction(success)) {
		  		success(data);
		  	}
		})
		.error(function(data){
			httpService.unmarkRequest("requestPayment");
			if (angular.isFunction(error)) {
				error(data);
			}
		});
	}

	httpService.updateProfile = function(profileData, success, error){
		httpService.markRequest("updateProfile");

		$http.post($rootScope.server_url+"api/profile/edit?key="+$rootScope.auth_key, profileData)
		.success(function(data){
		  	httpService.unmarkRequest("updateProfile");
		  	if (angular.isFunction(success)) {
		  		success(data);
		  	}
		})
		.error(function(data){
			httpService.unmarkRequest("updateProfile");
			if (angular.isFunction(error)) {
				error(data);
			}
		});
	}


	httpService.postCode = function(codeData, callback){
		httpService.markRequest("postCode");

		$http.post($rootScope.server_url+"api2/discountCode/create?key="+$rootScope.auth_key, codeData).success(function(data){
		  	httpService.unmarkRequest("postCode");
		  	if (angular.isFunction(callback)) {
		  		callback(data);
		  	}
		});
	}

	httpService.postMission = function(id_mission, missionData, callback){
		httpService.markRequest("postMission");

		$http.post($rootScope.server_url+"api2/missions/"+id_mission+"?key="+$rootScope.auth_key, missionData).success(function(data){
		  	httpService.unmarkRequest("postMission");
		  	if (angular.isFunction(callback)) {
		  		callback(data);
		  	}
		});
	}

	httpService.postSharedMission = function(id_mission, network, callback){
		httpService.markRequest("postSharedMission");

		$http.post($rootScope.server_url+"api2/missions/"+id_mission+"/share_completed?key="+$rootScope.auth_key, {
			network : network
		}).success(function(data){
			httpService.unmarkRequest("postSharedMission");
		  	if (angular.isFunction(callback)) {
		  		callback(data);
		  	}
		});
	}

	httpService.forgotPassword = function(email, callback){
		httpService.markRequest("forgotPassword");

		$http.post($rootScope.server_url+"api2/forgotPassword?key="+$rootScope.auth_key, {
			email : email
		}).success(function(data){
			httpService.unmarkRequest("forgotPassword");
		  	if (angular.isFunction(callback)) {
		  		callback(data);
		  	}
		});
	}

	httpService.resetPassword = function(resetData, callback){
		httpService.markRequest("resetPassword");

		$http.post($rootScope.server_url+"api2/resetPassword?key="+$rootScope.auth_key, resetData).success(function(data){
			httpService.unmarkRequest("resetPassword");
		  	if (angular.isFunction(callback)) {
		  		callback(data);
		  	}
		});
	}

	httpService.setInvitation = function(invitationData, success){

		$http.post($rootScope.server_url+"api2/setInvitation?key="+$rootScope.auth_key, invitationData).success(function(data){
		  	if (angular.isFunction(success)) {
		  		success(data);
		  	}
		});
	}

	// Initialize data from server
	// service - function to run
	// data - place requst only if $rootScope[data] is undefined
	// event - place request after event is triggered
	// arg - argument for request method
	httpService.init = function(service, data, event, arg){
		if (angular.isDefined($rootScope[data])) {
			return;
		}

		if (angular.isDefined(event) && event.length > 0) {

			if (event == "logged_in") {
				if (angular.isDefined($rootScope.logged_in) && $rootScope.logged_in) {
					httpService[service](arg);
				} else {
					$rootScope.$on(event, function(event, data){
						httpService[service](arg);
					});
				}
			}

			else if (event == "profile") {
				if (angular.isDefined($rootScope.profile)) {
					httpService[service](arg);
				} else {
					console.log(service+" is waiting for "+event);
					$rootScope.$on(event, function(event, data){
						httpService[service](arg);
					});
				}
			}
		} else {
			httpService[service](arg);
		}
	}

	return httpService;
});
