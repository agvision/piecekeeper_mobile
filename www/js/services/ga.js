
angular.module('starter')

.factory('GAService', function($window, $rootScope, $location) {
	var GAService = {};

	GAService.enableTracking = function(){
		if ($location.host() ==  "piecekeeper.onepiece.com") {
			return true;
		}

		return false;
	}

	GAService.trackPage = function(location){
		if (this.enableTracking()) {
			$window._gaq.push(['_trackPageview', location]);
		}
	}

	GAService.trackEvent = function(category, action, label, value){
		if (this.enableTracking()) {
			$window._gaq.push(['_trackEvent', category, action, label, value]);
		}
	}

	GAService.trackLoginEvent = function(action, label){
		this.trackEvent("Login", action, label, null);
	}

	GAService.trackRequestTime = function(action, label, value){
		this.trackEvent("Request", action, null, value);
	}

	return GAService;
});
